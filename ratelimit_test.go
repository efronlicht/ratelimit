package ratelimit_test

import (
	"testing"

	"gitlab.com/efronlicht/ratelimit"
)

func Test_Status_Parsing(t *testing.T) {
	for _, s := range []ratelimit.Status{ratelimit.STOP, ratelimit.GO} {
		got, err := ratelimit.ParseStatus(s.String())
		if err != nil {
			t.Fatal(err)
		}
		if got != s {
			t.Fatalf("expected %s, got %s", s, got)
		}
	}
}
