package ratelimit

import (
	"errors"
	"fmt"
	"strings"
)

const (
	// STOP is returned by a ratelimit.Interface when the request should stop.
	STOP Status = 0
	// GO is returned by a ratelimit.Interface when the request should proceed.
	GO Status = 1
)

type (
	// Status is STOP or GO. More kinds may be added in the future.
	Status byte

	// Interface represents a rate limiter. C
	Interface interface {
		// RateLimited returns GO if you can proceed, and STOP if you can't.
		RateLimited() (Status, error)
	}
)

var errUnknownStatus = errors.New(`unknown status: expected "STOP" or "GO"`)

func ParseStatus(raw string) (Status, error) {
	if strings.EqualFold(raw, "STOP") {
		return STOP, nil
	}
	if strings.EqualFold(raw, "GO") {
		return GO, nil
	}
	return STOP, errUnknownStatus
}

// String returns "STOP" or "GO".
func (s Status) String() string {
	switch s {
	case STOP:
		return "STOP"
	case GO:
		return "GO"
	default:
		return fmt.Sprintf("<unknown ratelimit.Status(%d)>", s)
	}
}
