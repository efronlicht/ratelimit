# ratelimit

Ratelimit provides a rate limiting mechanism.

## quickstart

### example - allow twenty http requests per minute to "localhost:8080/ping"
```go
import "gitlab.com/efronlicht/ratelimit"
var limiter = ratelimit.NewTokenBucket(context.TODO(), ratelimit.TokenBucketConfig {
    Tick: time.Minute,
    PerTick: 20, 
    MaxTokens: 100, 
    StartingTokens: 100,
})
var ping http.HandleFunc = func(w http.ResponseWriter, r *http.Request) {w.Write([]byte("ping"))}
func main() {
    http.Handle("/ping",  ratelimit.ApplyDefaultMiddleware(limiter, ping))})
    http.ListenAndServe(":8080", nil)
}
```