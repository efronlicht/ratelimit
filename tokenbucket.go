package ratelimit

import (
	"context"
	"errors"
	"fmt"
	"math"
	"sync/atomic"
	"time"
)

type (
	// TokenBucketConfig is the configuration for a TokenBucket, passed to NewTokenBucket during New().
	TokenBucketConfig struct {
		// Tick is how often to refresh PerTick tokens. Must be > 0.
		Tick time.Duration
		// PerTick is how many tokens to refresh every Tick. Must be > 0.
		PerTick uint64
		// MaxTokens to have in the bucket. Must be > 0.
		MaxTokens uint64
		// StartingTokens before the first Tick. Must be <= MaxTokens. Zero value is OK.
		StartingTokens uint64
	}
	// TokenBucket is a ratelimiter.Interface that uses a https://en.wikipedia.org/wiki/Token_bucket
	TokenBucket struct {
		tokens, max uint64
	}
)

// NewTokenBucket makes a new TokenBucket, which runs until the context is cancelled.
// It panics if cfg.Validate() would return an error, or if the ctx is nil.
// If you're not sure which context to use, use context.TODO().
// If you want it to run forever, use context.Background().
func NewTokenBucket(ctx context.Context, cfg TokenBucketConfig) *TokenBucket {
	if err := cfg.Validate(); err != nil {
		panic(fmt.Sprintf("NewTokenBucket: validating %T: %v", cfg, err))
	}
	if ctx == nil {
		panic("NewTokenBucket: nil ctx. (Use ctx.TODO() if you're not sure which context to use.)")
	}
	tb := &TokenBucket{tokens: cfg.StartingTokens, max: cfg.MaxTokens}
	go tb.fillBucket(ctx, cfg)
	return tb
}

// Validate the TokenBucketConfig. If Validate() would return a non-nil error, NewTokenBucket will panic.
func (lc TokenBucketConfig) Validate() error {
	switch {
	case lc.Tick <= 0:
		return errors.New("expected Tick > 0")
	case lc.PerTick == 0:
		return errors.New("expected PerTick > 0")
	case lc.MaxTokens == 0:
		return errors.New("expected MaxTokens > 0")
	case lc.StartingTokens > lc.MaxTokens:
		return fmt.Errorf("expected StartingTokens (%d) <= MaxTokens (%d)", lc.StartingTokens, lc.MaxTokens)
	default:
		return nil
	}
}

func (tb *TokenBucket) fillBucket(ctx context.Context, lc TokenBucketConfig) {
	ticker := time.NewTicker(lc.Tick)
	defer ticker.Stop()
	done := ctx.Done()
	for {
		select {
		case <-ticker.C:
			tb.addTokens(lc.PerTick, lc.MaxTokens)
		case <-done:
			return
		}
	}
}

// Tokens gets the count of the current tokens.
func (tb *TokenBucket) Tokens() uint64 {
	return atomic.LoadUint64(&tb.tokens)
}

func (tb *TokenBucket) compareAndSwap(old, new uint64) bool {
	return atomic.CompareAndSwapUint64(&tb.tokens, old, new)
}

func (tb *TokenBucket) addTokens(n, maxV uint64) (added uint64) {
	for {
		tokens := tb.Tokens()
		if tokens >= tb.max { // give up on adding: this is full.
			return 0
		}

		newV := min_u64(sat_add_uint64(tokens, n), maxV)
		swapped := tb.compareAndSwap(tokens, newV)
		if swapped { // we successfully added the tokens!
			return tokens - newV
		}
		// someone else did a CAS between when we loaded `old` and did our CAS.
		// we're still making forward progress, so keep trying.
		continue
	}
}

// getToken attempts to get a token. It is GO if it got one, and STOP if it isn't.
// This is the implementation of RateLimited.
func (tb *TokenBucket) getToken() Status {
	for {
		tokens := tb.Tokens()
		if tokens == 0 {
			return STOP
		}
		// we THINK there's a free token, so let's try and subtract exactly one.
		swapped := tb.compareAndSwap(tokens, tokens-1)
		if swapped { // we successfully obtained exactly one token!
			return GO
		}
		// someone else did a CAS between when we loaded `old` and did our CAS.
		// we're still making forward progress, so keep trying.
		continue
	}
}

// RateLimited implements ratelimit.Interface. RateLimitStatus is always STOP or GO, and error is always nil.
func (tb *TokenBucket) RateLimited() (Status, error) {
	return tb.getToken(), nil
}

func min_u64(a, b uint64) uint64 {
	if a < b {
		return a
	}
	return b
}

// add a+b, returning math.MaxUint64 if it would overflow.
func sat_add_uint64(a, b uint64) uint64 {
	sum := a + b
	if sum < a || sum < b {
		return math.MaxUint64
	}
	return sum
}
