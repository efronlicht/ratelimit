package ratelimit_test

import (
	"context"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/efronlicht/ratelimit"
)

// this test is nondeterministic depending on go's scheduler, etc.
// but I ran it 300 times and it seemed fine, so hey.
func Test_Bucket_Ratelimit(t *testing.T) {
	if testing.Short() {
		t.Skipf("SKIP %s: long, nondeterministic test", t.Name())
	}
	t.Logf("%s takes 2 seconds. you can disable it with the -short flag!", t.Name())
	t.Parallel()
	cfg := ratelimit.TokenBucketConfig{
		Tick:           100 * time.Millisecond,
		PerTick:        10,
		MaxTokens:      20,
		StartingTokens: 20,
	}
	const runtime = 2 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	bucket := ratelimit.NewTokenBucket(ctx, cfg)
	var sum uint64

	// start up something really racy.
	wg := new(sync.WaitGroup)
	wg.Add(50)
	for i := 0; i < 50; i++ {
		t := time.Tick(10 * time.Millisecond)
		go func() {
			n := uint64(0)
			defer func() {
				atomic.AddUint64(&sum, n)
				wg.Done()
			}()
			for {
				select {
				case <-ctx.Done():
					return
				case <-t:
					status, _ := bucket.RateLimited()
					if status == ratelimit.GO {
						n++
					}
				}
			}
		}()
	}
	wg.Wait()
	want := cfg.StartingTokens + cfg.PerTick*uint64(runtime/cfg.Tick)
	wantMin := want - 10
	wantMax := want + 10
	if sum < wantMin || sum > wantMax {
		t.Fatalf("expected between %d and %d", wantMin, wantMax)
	}
}

func Test_Cfg_Validate(t *testing.T) {
	t.Parallel()
	for name, cfg := range map[string]ratelimit.TokenBucketConfig{
		"0 tick":                     {Tick: +0, PerTick: 1, MaxTokens: 1, StartingTokens: 1},
		"negative tick":              {Tick: -1, PerTick: 1, MaxTokens: 1, StartingTokens: 1},
		"0 PerTick":                  {Tick: +1, PerTick: 0, MaxTokens: 1, StartingTokens: 1},
		"0 MaxTokens":                {Tick: +1, PerTick: 1, MaxTokens: 0, StartingTokens: 0},
		"StartingTokens > MaxTokens": {Tick: +1, PerTick: 1, MaxTokens: 1, StartingTokens: 2},
	} {
		t.Run(name+" returns err", func(t *testing.T) {
			if cfg.Validate() == nil {
				t.Error("expected an error")
			}
		})
	}
	goodCfg := ratelimit.TokenBucketConfig{Tick: time.Minute, PerTick: 1, MaxTokens: 5, StartingTokens: 5}
	if err := goodCfg.Validate(); err != nil {
		t.Error("expected no error, but got: ", err)
	}
}
