package ratelimitgin

import (
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/efronlicht/ratelimit"
)

type (
	mockRateLimiter struct {
		status ratelimit.Status
		err    error
	}
	panickingRateLimiter struct{}
)

var _, _ ratelimit.Interface = mockRateLimiter{}, panickingRateLimiter{}

func (mrl mockRateLimiter) RateLimited() (ratelimit.Status, error)  { return mrl.status, mrl.err }
func (panickingRateLimiter) RateLimited() (ratelimit.Status, error) { panic("uh oh!") }

var pingHandler http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("ping"))
}

func TestMain(m *testing.M) {
	gin.SetMode("release")
	gin.DefaultWriter = io.Discard
	gin.DefaultErrorWriter = io.Discard
	os.Exit(m.Run())
}

func Test_Middleware_WithDefaults(t *testing.T) {
	t.Parallel()
	for name, tt := range map[string]struct {
		limiterStatus  ratelimit.Status
		limiterError   error
		wantHTTPStatus int
	}{
		"not limited":           {ratelimit.GO, nil, 200},
		"limited":               {ratelimit.STOP, nil, http.StatusTooManyRequests},
		"limiter returns error": {ratelimit.STOP, errors.New("some error"), http.StatusInternalServerError},
	} {
		t.Run(name, func(t *testing.T) {
			w := httptest.NewRecorder()
			r := httptest.NewRequest("GET", "/ping", nil)
			router := gin.New()
			router.Use(Middleware(mockRateLimiter{status: tt.limiterStatus, err: tt.limiterError}, Config{}))
			router.GET("/ping", func(c *gin.Context) { c.Writer.Write([]byte("ping")) })
			router.ServeHTTP(w, r)
			if gotCode := w.Result().StatusCode; gotCode != tt.wantHTTPStatus {
				t.Errorf("expected status code %d, but got %d", tt.wantHTTPStatus, gotCode)
			}
		})
	}
	t.Run("skips", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/ping", nil)
		router := gin.New()
		router.Use(Middleware(mockRateLimiter{status: ratelimit.STOP, err: errors.New("some error")}, Config{SkipIf: func(r *http.Request) bool { return true }}))
		router.GET("/ping", func(c *gin.Context) { c.Writer.Write([]byte("ping")) })
		router.ServeHTTP(w, r)
		if gotCode := w.Result().StatusCode; gotCode != 200 {
			t.Fatal("expected 200")
		}
	})
}

func Test_Middleware_EdgeCases(t *testing.T) {
	t.Run("panics if nil limiter", func(t *testing.T) {
		defer func() {
			if p := recover(); p == nil {
				t.Fatal("expected a panic")
			}
		}()
		_ = Middleware(nil, Config{})
	})
}
