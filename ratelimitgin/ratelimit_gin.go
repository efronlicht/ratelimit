// Package ratelimitgin provides a customizable gin.Middleware to rate limit requests.
package ratelimitgin

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/efronlicht/ratelimit"
)

type (
	// middleware is a customizable rate-limiting middleware with sensible defaults, the equivalent of ratelimit.Middleware.
	// Build it with NewMiddleWare.
	middleware struct {
		// limiter is called at most one time per request, when when() returns true.
		limiter ratelimit.Interface
		// skipIf is when to skip rate limiting. defaults to SkipNever.
		skipIf func(r *http.Request) bool
		// errHandler is called on the error from Interface.RateLimited() if it's non-nil. Defaults to DefaultErrorHandler.
		errHandler func(c *gin.Context, err error)
		// stopHandler handles the request when the rate limit returns STOP. Defaults to DefaultStopHandler.
		stopHandler func(c *gin.Context)
	}
	// Config is used to customize a Middleware. The zero value is OK and ready for immediate use.
	Config struct {
		// SkipIf to apply rate limiting. Defaults to SkipNever.
		SkipIf func(r *http.Request) bool
		// ErrHandler is called on the error from Interface.RateLimited() if it's non-nil. Defaults to DefaultErrorHandler.
		ErrHandler func(c *gin.Context, err error)
		// StopHandler handles the request when the rate limit returns STOP. Defaults to DefaultStopHandler.
		StopHandler func(c *gin.Context)
	}
)

func (mw *middleware) HandlerFunc(c *gin.Context) {
	if mw.skipIf(c.Request) {
		c.Next()
		return
	}
	status, err := mw.limiter.RateLimited()
	if err != nil {
		mw.errHandler(c, err)
		return
	}
	if status == ratelimit.GO {
		c.Next()
		return
	}
	mw.stopHandler(c)
}

// Middleware builds acustomizable rate-limiting middleware with sensible defaults, the equivalent of ratelimit.Middleware.
// See Config for the options and defaults.
func Middleware(limiter ratelimit.Interface, cfg Config) gin.HandlerFunc {
	if limiter == nil {
		panic("nil limiter")
	}
	cfg = cfg.WithDefaults()
	return (&middleware{
		limiter:     limiter,
		skipIf:      cfg.SkipIf,
		errHandler:  cfg.ErrHandler,
		stopHandler: cfg.StopHandler,
	}).HandlerFunc
}

var ErrTooManyRequests = errors.New("too many requests")

func (cfg Config) WithDefaults() Config {
	if cfg.SkipIf == nil {
		cfg.SkipIf = func(r *http.Request) bool { return false }
	}
	if cfg.ErrHandler == nil {
		cfg.ErrHandler = DefaultErrorHandler
	}
	if cfg.StopHandler == nil {
		cfg.StopHandler = DefaultStopHandler
	}
	return cfg
}

// SkipNever returns false.
func SkipNever(r *http.Request) bool { return false }

// DefaultErrorHandler calls c.AbortWithError(http.StatusInternalServerError, err)
func DefaultErrorHandler(c *gin.Context, err error) {
	c.AbortWithError(http.StatusInternalServerError, err)
}

// DefaultStopHandler calls c.AbortWithError(http.StatusTooManyRequests, ErrTooManyRequests)
func DefaultStopHandler(c *gin.Context) {
	c.AbortWithError(http.StatusTooManyRequests, ErrTooManyRequests)
}
