package ratelimit_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/efronlicht/ratelimit"
)

type (
	mockRateLimiter struct {
		status ratelimit.Status
		err    error
	}
	panickingRateLimiter struct{}
)

var _, _ ratelimit.Interface = mockRateLimiter{}, panickingRateLimiter{}

func (mrl mockRateLimiter) RateLimited() (ratelimit.Status, error)  { return mrl.status, mrl.err }
func (panickingRateLimiter) RateLimited() (ratelimit.Status, error) { panic("uh oh!") }

var pingHandler http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("ping"))
}

func Test_Middleware_WithDefaults(t *testing.T) {
	t.Parallel()
	for name, tt := range map[string]struct {
		limiterStatus  ratelimit.Status
		limiterError   error
		wantHTTPStatus int
	}{
		"not limited":           {ratelimit.GO, nil, 200},
		"limited":               {ratelimit.STOP, nil, http.StatusTooManyRequests},
		"limiter returns error": {ratelimit.STOP, errors.New("some error"), http.StatusInternalServerError},
	} {
		t.Run(name, func(t *testing.T) {
			w := httptest.NewRecorder()
			r := httptest.NewRequest("GET", "/", nil)
			mw := ratelimit.ApplyNewMiddleware(mockRateLimiter{status: tt.limiterStatus, err: tt.limiterError}, pingHandler, ratelimit.MiddlewareConfig{})
			mw.ServeHTTP(w, r)
			if gotCode := w.Result().StatusCode; gotCode != tt.wantHTTPStatus {
				t.Errorf("expected status code %d, but got %d", tt.wantHTTPStatus, gotCode)
			}
		})
	}
}

func Test_Middleware_EdgeCases(t *testing.T) {
	for name, tt := range map[string]struct {
		limiter ratelimit.Interface
		handler http.Handler
	}{
		"no limiter": {handler: pingHandler},
		"no handler": {limiter: mockRateLimiter{}},
	} {
		t.Run(name, func(t *testing.T) {
			defer func() {
				if p := recover(); p == nil {
					t.Fatal("expected a panic")
				}
			}()
			_ = ratelimit.ApplyNewMiddleware(tt.limiter, tt.handler, ratelimit.MiddlewareConfig{})
		})
	}
}

func Test_Middleware_CustomBehavior(t *testing.T) {
	t.Parallel()

	customErrHandler := func(w http.ResponseWriter, r *http.Request, err error) {
		w.WriteHeader(400)
	}
	var customStopHandler http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(401)
	}
	for name, tt := range map[string]struct {
		limiterStatus  ratelimit.Status
		limiterError   error
		wantHTTPStatus int
	}{
		"not limited":           {ratelimit.GO, nil, 200},
		"limited":               {ratelimit.STOP, nil, 401},
		"limiter returns error": {ratelimit.STOP, errors.New("some error"), 400},
	} {
		t.Run(name, func(t *testing.T) {
			w := httptest.NewRecorder()
			r := httptest.NewRequest("GET", "/", nil)
			mw := ratelimit.ApplyNewMiddleware(mockRateLimiter{status: tt.limiterStatus, err: tt.limiterError}, pingHandler, ratelimit.MiddlewareConfig{
				StopHandler: customStopHandler,
				ErrHandler:  customErrHandler,
			})
			mw.ServeHTTP(w, r)
			if gotCode := w.Result().StatusCode; gotCode != tt.wantHTTPStatus {
				t.Errorf("expected status code %d, but got %d", tt.wantHTTPStatus, gotCode)
			}
		})
	}
	t.Run("skip", func(t *testing.T) {
		w := httptest.NewRecorder()
		r := httptest.NewRequest("GET", "/", nil)
		mw := ratelimit.ApplyNewMiddleware(panickingRateLimiter{}, pingHandler, ratelimit.MiddlewareConfig{
			SkipIf: func(r *http.Request) bool { return true },
		})
		mw.ServeHTTP(w, r)
		if w.Result().StatusCode != 200 {
			t.Error("expected to skip the rate limiter")
		}
	})
}
