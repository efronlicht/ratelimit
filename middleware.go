package ratelimit

import (
	"log"
	"net/http"
)

type (
	// Middleware is a customizable rate-limiting middleware with sensible defaults.
	// Wrap it around a http.Handler with NewMiddleware.
	// See gitlab.com/efronlicht/ratelimit/ratelimitgin for an equivalent that uses github.com/gin-gonic/gin.
	Middleware struct {
		// applied when when() returns false or the limiter returns GO.
		handler http.Handler
		// limiter is called at most one time per request, when when() returns true.
		limiter Interface
		// shouldSkip is when to skip rate limiting. defaults to SkipNever.
		shouldSkip func(r *http.Request) bool
		// errHandler handles the request when the rate limit returns an error. Defaults to DefaultErrHandler.
		errHandler func(w http.ResponseWriter, r *http.Request, err error)
		// stopHandler handles the request when the rate limit returns STOP. Defaults to WriteStatus(http.StatusTooManyRequests).
		stopHandler http.Handler
	}

	// MiddlewareConfig is used to customize a middleware. The zero value is OK and ready for immediate use.
	MiddlewareConfig struct {
		// SkipIf to apply rate limiting. Defaults to RateLimitAlways.
		SkipIf func(r *http.Request) bool
		// ErrHandler handles the request when the rate limit returns an error. Defaults to DefaultErrHandler
		ErrHandler func(w http.ResponseWriter, r *http.Request, err error)
		// StopHandler handles the request when the rate limit returns STOP. Defaults to WriteStatus(http.StatusTooManyRequests).
		StopHandler http.Handler
	}
)

// ApplyNewMiddleware makes a new MiddleWare using the provided MiddlewareConfig. Panics if limiter or handler are nil.
// The default behavior is as follows:
// 	//  rate limit every request.
// 	// - if the rate limiter returns an error, write 500 Internal Server Error.
// 	// - if the rate limiter returns STOP, write 429 Too Many Requests.
// 	// - otherwise, proceed with the provided handler.
func ApplyNewMiddleware(limiter Interface, handler http.Handler, cfg MiddlewareConfig) *Middleware {
	cfg = cfg.WithDefaults()
	if handler == nil {
		panic("building *ratelimit.Middleware: nil handler")
	}
	if limiter == nil {
		panic("building *ratelimit.Middleware: nil limiter")
	}
	return &Middleware{
		errHandler:  cfg.ErrHandler,
		handler:     handler,
		limiter:     limiter,
		stopHandler: cfg.StopHandler,
		shouldSkip:  cfg.SkipIf,
	}
}

func ApplyDefaultMiddleware(limiter Interface, handler http.Handler) *Middleware {
	return ApplyNewMiddleware(limiter, handler, MiddlewareConfig{})
}

// ServeHTTP implements http.Handler.
//	// - if the rate limiter shouldSkip(), proceed with the provided handler.
// 	// - if the rate limiter returns an error, call the ErrHandler.
// 	// - if the rate limiter returns STOP, call the StopHandler.
// 	// - otherwise, proceed with the provided handler.
func (cm *Middleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if cm.shouldSkip(r) {
		cm.handler.ServeHTTP(w, r)
	} else if status, err := cm.limiter.RateLimited(); err != nil {
		cm.errHandler(w, r, err)
	} else if status == GO {
		cm.handler.ServeHTTP(w, r)
	} else {
		cm.stopHandler.ServeHTTP(w, r)
	}
}

// DefaultStopHandler is uses as the zero value for a MiddlewareConfig.StopHandler.
// It writes http.StatusTooManyRequests.
func DefaultStopHandler(w http.ResponseWriter, _ *http.Request) {
	http.Error(w, "429 Too Many Requests", http.StatusTooManyRequests)
}

// SkipNever always returns false.
func SkipNever(r *http.Request) bool { return false }

func (mc MiddlewareConfig) WithDefaults() MiddlewareConfig {
	if mc.StopHandler == nil {
		mc.StopHandler = http.HandlerFunc(DefaultStopHandler)
	}
	if mc.ErrHandler == nil {
		mc.ErrHandler = DefaultErrHandler
	}
	if mc.SkipIf == nil {
		mc.SkipIf = SkipNever
	}
	return mc
}

// DefaultErrHandler is used as the zero value for a MiddlewareConfig.ErrorHandler.
// It logs the error, then serves a 500 Internal Server Error.
func DefaultErrHandler(w http.ResponseWriter, r *http.Request, err error) {
	log.Printf("error while serving http method %q path %q: ratelimiter returned error: %v", r.Method, r.URL.Path, err)
	http.Error(w, "500 Internal Server Error", http.StatusInternalServerError)
}
